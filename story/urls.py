from django.contrib import admin
from django.urls import path, include

urlpatterns = [
    path('admin/', admin.site.urls),
    path('',include('homepage.urls')),
    path('cerita/',include('cerita.urls')),
    path('books/',include('books.urls')),
]

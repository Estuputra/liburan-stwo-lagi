
$(document).ready(() => {
    $(window).scroll(function(){
        let wScroll = $(this).scrollTop();
        //Education
        
        if(wScroll > $('.img-fluid').offset().top ){
            $('.education').addClass('muncul');
        } else if(wScroll < $('.img-fluid').offset().top ){
            $('.education').removeClass('muncul');
        }

        if(wScroll >= $('.foto').offset().top){
            $('.img-fluid').addClass('muncul');
        }else if(wScroll <= $('.foto').offset().top ){
            $('.img-fluid').removeClass('muncul');
        }

        if(wScroll > $('.img-fluid').offset().top + 400 ){
            $('.panitia').addClass('muncul');
        } else if(wScroll < $('.img-fluid').offset().top + 400 ){
            $('.panitia').removeClass('muncul');
        }

        if(wScroll > $('.work  .titleWork').offset().top - 680){
            $('.titleWork').addClass('muncul');
        } else if(wScroll < $('.work  .titleWork').offset().top - 680){
            $('.titleWork').removeClass('muncul');
        }

        
        if(wScroll > $('.work .SA').offset().top - 650) {
            $('.juli').addClass('after');
            $('.oktober').addClass('after');
        } else if(wScroll < $('.work  .SA').offset().top - 650){
            $('.juli').removeClass('after');
            $('.oktober').removeClass('after');
        }

    });

    $('.aJourney').click((e) =>{
        e.preventDefault();
        let a = $('.foto').offset().top;
        $('html, body').animate({
            scrollTop: a
        }, 1000);
    });

    
    
    
    
    $('.H').css("color","#A9C4FA");
    $('.ai').css("color","#2855A4");
    $('.laptop').addClass('after');

    // Warna si H
    $('.H').fadeOut("slow",() => {
        $('.H').css("color","#2855A4");
        $('.H').fadeIn("slow",() =>{
            $('.H').fadeOut("slow",() =>{
                $('.H').css("color","#A9C4FA");
                $('.H').fadeIn("slow");
            })
        });
    });

    // // Warna si ai
    $('.ai').fadeOut("slow", ()=>{
        $('.ai').css("color","#2855A4");
        $('.ai').fadeIn("slow", ()=>{
            $('.ai').fadeOut("slow", ()=>{
                $('.ai').css("color","#A9C4FA");
                $('.ai').fadeIn("slow")
            });
        });
    });

    // Warna nama user
    $('.namaUser').css("color","#A9C4FA");
    
    let waktu = parseInt($('.waktu').text().substring(0,2));
    if(waktu > 18 && waktu <=24){
        $(".greetingText").delay(4000).fadeOut("slow",()=>{
            $('.saturnus').css("top","40%");
            $('.saturnus').css("left","75%");
            $('.bulan').css("top","10%");
            $('.bulan').css("left","20%");
            $(".greetingText").css("color","#A9C4FA");
            $(".greetingText").css("margin","auto");
            $(".greetingText").css("font-size","250%");
            $(".greetingText").text("Selamat malam!!!!");
           
            $(".greetingText").fadeIn("slow",()=>{
                $(".buttonJourney").addClass("timbul");
            });
        });
    } else if(waktu >= 0  && waktu < 06){
        $(".greetingText").delay(4000).fadeOut("slow",()=>{
            $('.saturnus').css("top","40%");
            $('.saturnus').css("left","75%");
            $('.bulan').css("top","10%");
            $('.bulan').css("left","20%");
            $(".greetingText").css("color","#A9C4FA");
            $(".greetingText").css("margin","auto");
            $(".greetingText").css("font-size","250%");
            $(".greetingText").text("Selamat tahajjud!");
           
            $(".greetingText").fadeIn("slow",()=>{
                $(".buttonJourney").addClass("timbul");
            });
        });
    } else if(waktu >= 06  && waktu <= 10){
        $(".greetingText").delay(4000).fadeOut("slow",()=>{
            $('.saturnus').css("top","40%");
            $('.saturnus').css("left","75%");
            $('.bulan').css("top","10%");
            $('.bulan').css("left","20%");
            $(".greetingText").css("color","#A9C4FA");
            $(".greetingText").css("margin","auto");
            $(".greetingText").css("font-size","250%");
            $(".greetingText").text("Selamat pagi!!!!!");
           
            $(".greetingText").fadeIn("slow",()=>{
                $(".buttonJourney").addClass("timbul");
            });
        });
    }else if(waktu > 10  && waktu <= 15){
        $(".greetingText").delay(4000).fadeOut("slow",()=>{
            $('.saturnus').css("top","40%");
            $('.saturnus').css("left","75%");
            $('.bulan').css("top","10%");
            $('.bulan').css("left","20%");
            $(".greetingText").css("color","#A9C4FA");
            $(".greetingText").css("margin","auto");
            $(".greetingText").css("font-size","250%");
            $(".greetingText").text("Selamat siang!!!!");
           
            $(".greetingText").fadeIn("slow",()=>{
                $(".buttonJourney").addClass("timbul");
            });
        });
        
    } else if(waktu > 15  && waktu <= 18){
        $(".greetingText").delay(4000).fadeOut("slow",()=>{
            $('.saturnus').css("top","40%");
            $('.saturnus').css("left","75%");
            $('.bulan').css("top","10%");
            $('.bulan').css("left","20%");
            $(".greetingText").css("color","#A9C4FA");
            $(".greetingText").css("margin","auto");
            $(".greetingText").css("font-size","250%");
            $(".greetingText").text("Selamat sore!!!!!");
           
            $(".greetingText").fadeIn("slow",()=>{
                $(".buttonJourney").addClass("timbul");
            });
        });
    }
});


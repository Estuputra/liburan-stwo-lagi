$(document).ready(function(){

    
    $("#theme_switch").on('click',() =>{
        let body = document.querySelector('body');
        if(body.classList.contains('light')){
            body.classList.remove('light');
            body.classList.add('dark');
            $("#kartu").addClass("bg-secondary");
            $("#navbar").removeClass("bg-light ");
            $("#navbar").removeClass("navbar-light ");
            $("#navbar").addClass("bg-dark");
            $("#navbar").addClass("navbar-dark");
            $("#buttonCerita").addClass("dark");
            $("#postingan").addClass("text-black-50");
            $("#postinganBody").addClass("bg-dark");
            $("#postinganBody").addClass("text-white");
        } else{
            body.classList.remove('dark');
            body.classList.add('light');
            $("#kartu").removeClass("bg-secondary");
            $("#navbar").removeClass("bg-dark ");
            $("#navbar").removeClass("navbar-dark ");
            $("#navbar").addClass("bg-light");
            $("#navbar").addClass("navbar-light");
            $("#buttonCerita").removeClass("dark");
        }
    });

});
   

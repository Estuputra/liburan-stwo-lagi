$(document).ready(() => {
    $('#theme_switch').on('click',() =>{
        let body = document.querySelector('body');
        if(body.classList.contains('light')){
            body.classList.remove('light');
            body.classList.add('dark');
            $("#cardForm").addClass("bg-secondary");
            $("#buttonCerita").addClass("dark");
            $("#postingan").css("color","black");
            $("#postinganBody").addClass("bg-secondary");
            $("#postinganBody").addClass("text-black");
            $("#postinganBody").addClass("dark");
            $("#postinganBody").removeClass("light");
            $(".urName").css("color","black");
            $(".card-header").addClass("dark");
           
        }else{
            body.classList.remove('dark');
            body.classList.add('light');
            $("#cardForm").removeClass("bg-secondary");
            $("#buttonCerita").removeClass("dark");
            $("#postinganBody").removeClass("bg-secondary");
            $("#postinganBody").removeClass("text-black");
            $(".urName").css("color","white");
            $("#postinganBody").removeClass("dark");
            $("#postinganBody").addClass("light");
            $(".card-header").removeClass("dark");
        }
    });
});

$(document).ready(() => {
    $(window).scroll(function(){
        let wScroll = $(this).scrollTop();
        //Education
        if(wScroll > $('.img-fluid').offset().top ){
            $('.education').addClass('muncul');
        } else if(wScroll < $('.img-fluid').offset().top ){
            $('.education').removeClass('muncul');
        }

        if(wScroll > $('.containerOur').offset().top){
            $('.img-fluid').addClass('muncul');
        }else if(wScroll < $('.containerOur').offset().top ){
            $('.img-fluid').removeClass('muncul');
        }

        if(wScroll > $('.img-fluid').offset().top + 400 ){
            $('.panitia').addClass('muncul');
        } else if(wScroll < $('.img-fluid').offset().top + 400 ){
            $('.panitia').removeClass('muncul');
        }
    });

    $('.aJourney').click((e) =>{
        if (this.hash !== "") {
            // Prevent default anchor click behavior
            event.preventDefault();
      
            // Store hash
            var hash = this.hash;
      
            // Using jQuery's animate() method to add smooth page scroll
            // The optional number (800) specifies the number of milliseconds it takes to scroll to the specified area
            $('html, body').animate({
              scrollTop: $(hash).offset().top
            }, 800, function(){
      
              // Add hash (#) to URL when done scrolling (default click behavior)
              window.location.hash = hash;
            });
          } // E
    });

});


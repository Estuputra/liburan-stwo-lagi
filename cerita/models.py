from django.db import models

# Create your models here.

class Cerita(models.Model):
    pencerita = models.CharField(max_length = 30)
    cerita = models.CharField(max_length = 2000)
    waktu_posting = models.DateTimeField(auto_now_add=True)
    def __str__(self):
        return self.pencerita
    

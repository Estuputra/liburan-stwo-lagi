from django.shortcuts import render
from django.http import HttpResponseRedirect

from .forms import FormCerita
from .models import Cerita
# Create your views here.
def index(request):
    form = FormCerita()

    if request.method == 'POST':
        form_input = FormCerita(request.POST)
        if form_input.is_valid():
            data = form_input.cleaned_data
            data_input = Cerita()
            data_input.pencerita = data['pencerita']
            data_input.cerita = data['cerita']
            data_input.save()
            current_data = Cerita.objects.all()
            
            return render(request, 'cerita.html',{'form':form, 'status':'success','data':current_data})
        else:
            current_data = Cerita.objects.all()
            return render(request, 'cerita.html',{'form':form, 'status':'failed','data':current_data})
 


    else:
        current_data = Cerita.objects.all()
        return render(request,'cerita.html',{'form':form,'data':current_data})

# def createCerita(request):
#     context = {
#         'formCerita': FormCerita(),
#     }
#     if request.method == 'POST':
#         Cerita.objects.create(
#             pencerita = request.POST.get('pencerita'),
#             cerita = request.POST.get('cerita'),
#         )
#         print(Cerita.objects.all())
#         return HttpResponseRedirect("/cerita")

#     return render(request,'cerita/create.html',context)

    
from django import forms

class FormCerita(forms.Form):
    """FORMNAME definition."""

    # TODO: Define form fields here
    pencerita = forms.CharField(
        label = "Nama Lengkap", 
        max_length = 30,
        widget = forms.TextInput(
            attrs={
                'class':'form-control',
                'placeholder':'Hai, nama kamu siapa?'
            }
        )
    )
    cerita = forms.CharField(
        label = "Cerita hari ini", 
        widget = forms.Textarea(
            attrs={
                'class':'form-control',
                'placeholder':'Apa cerita mu hari ini?'
            }
        ), 
        max_length=2000,
    )
    
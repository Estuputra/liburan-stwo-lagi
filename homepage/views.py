from django.shortcuts import render, redirect
from django.http import HttpResponseRedirect
from django.utils import timezone, dateformat
from django.contrib.auth import authenticate, login, logout
from django.contrib.auth.models import User



# Create your views here.

from .forms import FormNamaDepan, FormLogin
# from .models import User
def index(request):
    
   
    context ={
        'heading':'Welcome bro',
        'subheading':'Enjoy!'
    }
    return render(request,'index.html',context)

def reqNama(request):
    form = FormNamaDepan()
    formatted_date = timezone.now()

    if request.method == 'POST':
        form_input = FormNamaDepan(request.POST)
        if form_input.is_valid():
            data = form_input.cleaned_data
            data_input = User()
            data_input.nama = data['nama']
            data_input.save()
            return render(request, 'index.html',{'form':form,'user':data_input,'status':'success','waktu': formatted_date,'heading':'Welcome bro',
        'subheading':'Enjoy!'})
        else:
            return render(request,'reqNama.html',{'form':form,'user':data_input,'status':'failed'})
    else:
        return render(request,'reqNama.html',{'form':form})

def loginViews(request):
    formLogin = FormLogin()
    formatted_date = timezone.now()
    
    if request.method == "POST":
        username_login = request.POST['username']
        password_login = request.POST['password']
        
        user = authenticate(request, username =username_login, password = password_login)

        if user is not None:
            login(request,user)
            # print(user)
            return render(request, 'index.html',{'form':formLogin,'status':'success','waktu': formatted_date,'heading':'Welcome bro',
        'subheading':'Enjoy!'})
        else:
            return render(request,'reqNama.html',{'form':formLogin,'status':'gagal'})
    if request.method == "GET":
        if request.user.is_authenticated:
            return render(request, 'index.html',{'form':formLogin,'status':'success','waktu': formatted_date,'heading':'Welcome bro',
        'subheading':'Enjoy!'})
        else:
            return render(request,'reqNama.html',{'form':formLogin,'listUser':User.objects.all()})

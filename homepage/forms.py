from django import forms

class FormNamaDepan(forms.Form):
    nama = forms.CharField(
        label = "Ur name:",
        max_length=30,
        widget = forms.TextInput(
            attrs={
                'class':'form-control',
                'placeholder':'Isi nama kamu!',
            }
        )
    )

class FormLogin(forms.Form):
    username = forms.CharField(
        label = "Username:",
        max_length=20,
        widget = forms.TextInput(
            attrs={
                'class':'form-control',
                'placeholder':'Isi username mu bos',
                'name':'username',
            }
        )
    )

    password = forms.CharField(
        label = "Password:",
        widget = forms.PasswordInput(
            attrs={
                'class':'form-control',
                'placeholder':'Isi password mu bos',
                'name':'password',
            }
        )
    )    